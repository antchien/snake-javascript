(function(root) {
  var SnakeGame = root.SnakeGame = (root || {});

  var View = SnakeGame.View = function(el) {
    this.$el = el
  }

  View.prototype.start = function() {
    this.grid = new SnakeGame.Board(10);
    var view = this;

    var board = this.grid
    var id = setInterval( function() {
      view.$el.off();
      view.$el.on("keydown", function(e) {
        switch(e.keyCode) {
        case 38:
          board.snake.turn('N');
          break;
        case 40:
          //down
          board.snake.turn('S');
          break;
        case 37:
          //left
          board.snake.turn('W');
          break;
        case 39:
          //right
          board.snake.turn('E');
          break;
        }
      });
      board.snake.move();
      if (board.snake.gameOver()) {
        board.snake.segments = []
        window.clearInterval(id);
        alert("You lose, loser!");
      }
      view.$el.html(board.render.bind(board));
    }, 500)
  }

})(this);

var v = new View($('body'));
v.start();