(function(root) {
  var SnakeGame = root.SnakeGame = (root || {});

  var Snake = SnakeGame.Snake = function(board) {
    this.board = board;
    this.dir = "N"
    this.segments = [new Coord(3,3)];
  }

  Snake.prototype.move = function() {
    var oldHead = this.segments[0];

    var tail = this.segments.pop();

    var newSeg = new Coord(oldHead.x, oldHead.y);

    newSeg.plus(this.dir)
    this.segments.unshift(newSeg);

    var board = this.board;
    var snake = this;
    board.apples.forEach( function(coord) {
      if (newSeg.isEqual(coord)) {
        board.apples.pop();
        board.randApple();
        snake.grow(tail);
      }
    });
  }

  Snake.prototype.gameOver = function() {
    var head = this.segments[0];

    for( x = 1; x < this.segments.length; x++ ) {
      if (this.segments[x].isEqual(head)) {
        return true;
      }
    }

    return !(head.onBoard());
  }

  Snake.prototype.turn = function(dir) {
    this.dir = dir;
  };

  Snake.prototype.grow = function(newTail) {
    this.segments.push(newTail);
  };

  var Coord = SnakeGame.Coord = function(x, y) {
    this.x = x;
    this.y = y;
  }

  Coord.prototype.plus = function(dir) {
    switch(dir) {
    case "N":
      this.x -= 1;
      break;
    case "S":
      this.x += 1;
      break;
    case "E":
      this.y += 1;
      break;
    case "W":
      this.y -= 1;
      break;
    }
  }

  Coord.prototype.isEqual = function(otherCoord) {
    return this.x === otherCoord.x && this.y === otherCoord.y;
  }

  Coord.prototype.onBoard = function() {
    if (this.x < 0 || this.x > 10) {
      return false;
    }

    if (this.y < 0 || this.y > 10) {
      return false;
    }

    return true;
  };


  var Board = SnakeGame.Board = function(size) {
    this.apples = [];
    this.grid = []
    for(var i = 0; i < size; i++) {
      var newRow = [];
      for(var j = 0; j < size; j++) {
        newRow.push('.');
      };

      this.grid.push(newRow);
    };

    this.apples = [];
    this.snake = new SnakeGame.Snake(this);

    this.randApple();
  };

  Board.prototype.randApple = function() {
    var x = Math.floor(Math.random() * this.grid.length);
    var y = Math.floor(Math.random() * this.grid.length);
    newCoord = new Coord(x, y);
    var board = this;
    var appleplaced = true
    this.snake.segments.forEach( function(coord) {
      if (newCoord.isEqual(coord)) {

        appleplaced = false;
      }
    });
    if(appleplaced) {
      this.apples.push(newCoord);
    } else {
      this.randApple();
    }
  }

  Board.prototype.render = function() {
    $('body').html('snake');
    var $container = $('<ul></ul>')

    for(var i = 0; i < this.grid.length; i++) {

      for(var j = 0; j < this.grid.length; j++) {
        var $cell = $('<li class="cell"></li>');

        var newSeg = new Coord(i, j);

        this.snake.segments.forEach( function(coord) {
          if (newSeg.isEqual(coord)) {
            $cell.addClass('snake');
          }
        });

        this.apples.forEach( function(coord) {
          if (newSeg.isEqual(coord)) {
            $cell.addClass('apple');
          }
        });

        $container.append($cell);

      };

      $('body').append($container);
    };
  };

})(this)